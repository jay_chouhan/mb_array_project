const lastCarInventory = function(inventory){
    if(inventory.length!==0){
        return `Last car is a ${inventory[inventory.length-1].car_make} ${inventory[inventory.length-1].car_model}.`;
    }
    else{
        return 'Empty Inventory';
    }
}

module.exports=lastCarInventory;