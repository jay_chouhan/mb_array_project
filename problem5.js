const carYears = require('./problem4');

const oldCars=(inventory,year)=>{
    const years = carYears(inventory)
    const oldArr = []
    for(let i=0; i<years.length; i++){
        if(years[i]<year){
            oldArr.push(inventory[i]);
        }
    }
    return oldArr;
}

module.exports=oldCars;