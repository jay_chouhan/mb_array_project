const alphabetSort = (inventory) =>{
    return inventory.sort((x,y)=>{
        let nameX = x.car_model.toUpperCase();
        let nameY = y.car_model.toUpperCase();
        if(nameX<nameY){
            return -1;
        }
        if(nameX>nameY){
            return 1;
        }
        return 0 //if names are equal
    });
};

module.exports=alphabetSort;