const carInfo= function(inventory,carId){
    for(let item = 0; item <inventory.length; item++){
        if(inventory[item]['id']===carId){
            return `Car ${carId} is a ${inventory[item].car_year} ${inventory[item].car_make} ${inventory[item].car_model}`;
        }
    }
    return 'Car not found'
}
module.exports=carInfo;